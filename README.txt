This module provides a wrapper for the Ultimate Fade-in Slideshow found on 
http://www.dynamicdrive.com/dynamicindex14/fadeinslideshow.htm

This module has been coded to work with v1.51 of the Ultimate Fade-in Slideshow
module.  

The Ultimate Fade-in Slideshow is a robust, cross browser fade in slideshow script that 
incorporates the following key features. The ability to display multiple slideshows on 
the same page, The ability to randomize the display order of the images. Each slideshow 
retrieves it's images from a directory that you specify in the module configuration pages. 
You can also set the slideshow to pause when the mouse moves over it. Each instance of a 
slideshow on the page is completely independent of the other, meaning the aforementioned 
features can be customized for each slideshow. The cool fading effect works in IE, 
NS6+/ Firefox, and Safari 2, while all other browsers such as Opera 8 will simply 
skip the effect but continue to display the images.  

Note: This module does not support linking and link targeting of images but this feature
can be implemented by follwing the directions on the dynamicdrive.com website as the
underlying Fade-in Slideshow script supports these features.

Installation/Use:

(steps 2 and 3 are needed due to license restrictions on both the dynamicdrive site and
drupal.org)

1) Install the module as normal by placing it in your sites module directory.
2) copy the javascript found in step one on page http://www.dynamicdrive.com/dynamicindex14/fadeinslideshow.htm
3) paste the javascript copied above into the file named fade_slideshow_js_inc file
4) Enable the module at /admin/build/modules
5) Configure the module at admin/settings/fade_slideshow

This module supports up to 6 slideshows for your site. Complete the section(s) 
at admin/settings/fade_slideshow for each slideshow you want to implement and then 
use the php code provided to embed your slideshow in a template, node or block.

To Do:
1) Allow embeding via filters

